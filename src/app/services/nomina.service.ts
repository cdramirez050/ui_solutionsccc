import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { Nomina } from "../pages/nomina/interface/nomina";

@Injectable({
    providedIn: 'root'
  })
export class NominaService {

    constructor(
        private httpClient : HttpClient,
    ){}

    saveNomina(nomina : Nomina) {
        return this.httpClient.post(environment._API_SOLUTIONSCCC + '/nomina', nomina);
      }
    
    getByNombre(nombre : string){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/nomina/nombre/' + nombre);
    }

    getAllNomina(){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/nomina/all');
    }

    getById(nomina: Nomina){
      return this.httpClient.get(environment._API_SOLUTIONSCCC + '/nomina/' + nomina);
    }

    updateCliente(nomina : Nomina){
      return this.httpClient.put(environment._API_SOLUTIONSCCC + '/nomina/' + nomina.idNomina, nomina);
    }

    deleteNomina(nomina : Nomina){
      return this.httpClient.delete(environment._API_SOLUTIONSCCC + '/nomina/' + nomina.idNomina);
  }
    
}