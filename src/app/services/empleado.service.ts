import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { Empleado } from '../pages/empleado/interface/empleado';

@Injectable({
    providedIn: 'root'
  })
export class EmpleadoService {

    constructor(
        private httpEmpleado : HttpClient,
    ){}

    saveEmpleado(empleado : Empleado) {
        return this.httpEmpleado.post(environment._API_SOLUTIONSCCC + '/empleado', empleado);
      }
    
    getByNumeroDocumentoEmpleado(numeroDocumentoEmp : string){
        return this.httpEmpleado.get(environment._API_SOLUTIONSCCC + '/empleado/numeroDocumento/' + numeroDocumentoEmp);
    }

    getByNombreEmpleado(nombreEmpleado : string){
        return this.httpEmpleado.get(environment._API_SOLUTIONSCCC + '/empleado/nombres/' + nombreEmpleado);
    }

    getAllEmpleado(){
        return this.httpEmpleado.get(environment._API_SOLUTIONSCCC + '/empleado/all');
    }

    updateEmpleado(empleado : Empleado){
      return this.httpEmpleado.put(environment._API_SOLUTIONSCCC + '/empleado/' + empleado.idEmpleado, empleado);
    }

    deleteEmpleado(empleado : Empleado){
        return this.httpEmpleado.delete(environment._API_SOLUTIONSCCC + '/empleado/' + empleado.idEmpleado);
    }

}