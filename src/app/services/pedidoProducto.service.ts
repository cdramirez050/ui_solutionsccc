import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { VentaProducto } from "../pages/producto-pedido/interface/ventaProducto";

@Injectable({
    providedIn: 'root'
  })
export class PedidoProductoService {

    constructor(
        private httpClient : HttpClient,
    ){}

    saveCantidadProducto(cantidadProducto : VentaProducto) {
        return this.httpClient.post(environment._API_SOLUTIONSCCC + '/ventaproducto', cantidadProducto);
      }
    
    getAllCantidadProducto(){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/ventaproducto/all');
    }

    updateCantidadProducto(cantidadProducto : VentaProducto){
      return this.httpClient.put(environment._API_SOLUTIONSCCC + '/ventaproducto/' + cantidadProducto.idVentaProducto, cantidadProducto);
    }

    deleteCantidadProducto(cantidadProducto : VentaProducto){
        return this.httpClient.delete(environment._API_SOLUTIONSCCC + '/stock/' + cantidadProducto.idVentaProducto);
    }
    
}