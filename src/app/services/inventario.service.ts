import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { Inventario } from "../pages/inventario/interface/inventario";

@Injectable({
    providedIn: 'root'
  })
export class InventarioService {

    constructor(
        private httpClient : HttpClient,
    ){}

    saveInventario(inventario : Inventario) {
        return this.httpClient.post(environment._API_SOLUTIONSCCC + '/inventario', inventario);
    }

    getAllInventario(){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/inventario/all');
    }

    deleteInventario(inventario : Inventario){
        return this.httpClient.delete(environment._API_SOLUTIONSCCC + '/inventario/' + inventario.idInventario);
    }
    
}