import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { Pedido } from "../pages/pedido/interface/pedido";

@Injectable({
    providedIn: 'root'
  })
export class PedidoService {

    constructor(
        private httpClient : HttpClient,
    ){}

    savePedido(pedido : Pedido) {
        return this.httpClient.post(environment._API_SOLUTIONSCCC + '/pedido', pedido);
      }
    
    getByFechaPedido(fechaPedido : string){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/pedido/fechaPedido/' + fechaPedido);
    }

    getByFechaEntrega(fechaEntrega : string){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/pedido/fechaEntrega/' + fechaEntrega);
    }

    getByFechaDescripcion(descripcion : string){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/pedido/descripcion/' + descripcion);
    }

    getAllPedido(){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/pedido/all');
    }

    getByIdPedido(pedido: Pedido){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/pedido/' + pedido.idPedido);
    }

    updatePedido(pedido : Pedido){
      return this.httpClient.put(environment._API_SOLUTIONSCCC + '/pedido/' + pedido.idPedido, pedido);
    }

    deletePedido(pedido : Pedido){
        return this.httpClient.delete(environment._API_SOLUTIONSCCC + '/pedido/' + pedido.idPedido);
    }
    
}