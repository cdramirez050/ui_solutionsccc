import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { Loggin } from "../pages/loggin/loggi/loggin";
import { Nomina } from "../pages/nomina/interface/nomina";

@Injectable({
    providedIn: 'root'
  })
export class LogginService {

    public isActiveSection: boolean = false;

    constructor(
        private httpClient : HttpClient,
    ){}

    getByPassword(id : number){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/login/' + id);
    }

    updatePassword(password : Loggin){
      return this.httpClient.put(environment._API_SOLUTIONSCCC + '/login/' + password.idLoggin, password);
    }

    activeSection(activeSection: boolean){
        this.isActiveSection = activeSection;
    }
    
}