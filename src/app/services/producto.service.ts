import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { Producto } from "../pages/producto/interface/producto";

@Injectable({
    providedIn: 'root'
  })
export class ProductoService {

    constructor(
        private httpClient : HttpClient,
    ){}

    saveProducto(producto : Producto) {
        return this.httpClient.post(environment._API_SOLUTIONSCCC + '/producto', producto);
      }
    
    getByFechaCompra(fechaCompra : string){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/producto/fechaCompra/' + fechaCompra);
    }

    getByNombre(nombre : string){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/producto/nombre/' + nombre);
    }

    getById(producto : Producto){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/producto/' + producto);
    }

    getAllProducto(){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/producto/all');
    }

    updateProducto(producto : Producto){
      return this.httpClient.put(environment._API_SOLUTIONSCCC + '/producto/' + producto.idProducto, producto);
    }

    deleteProducto(producto : Producto){
        return this.httpClient.delete(environment._API_SOLUTIONSCCC + '/producto/' + producto.idProducto);
    }
}