import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { ProductoPedido } from "../pages/producto-pedido/interface/productoPedido";

@Injectable({
    providedIn: 'root'
  })
export class ventaProducto {

    constructor(
        private httpClient : HttpClient,
    ){}

    savePedidoProducto(productoPedido : ProductoPedido) {
        return this.httpClient.post(environment._API_SOLUTIONSCCC + '/stock', productoPedido);
    }
    
    getAllPedidoProducto(){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/stock/all');
    }

    deletePedidoProducto(pedidoProducto : ProductoPedido){
        return this.httpClient.delete(environment._API_SOLUTIONSCCC + '/stock/' + pedidoProducto.idStock);
    }
    
}