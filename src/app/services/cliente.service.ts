import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../environments/environment';
import { Cliente } from '../pages/cliente/interface/cliente';

@Injectable({
    providedIn: 'root'
  })
export class ClienteService {

    constructor(
        private httpClient : HttpClient,
    ){}

    saveCliente(cliente : Cliente) {
        return this.httpClient.post(environment._API_SOLUTIONSCCC + '/cliente', cliente);
      }
    
    getByNumeroDocumento(numeroDocumento : string){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/cliente/numeroDocumento/' + numeroDocumento);
    }

    getByNombreCliente(nombre : string){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/cliente/nombres/' + nombre);
    }

    getAllClientes(){
        return this.httpClient.get(environment._API_SOLUTIONSCCC + '/cliente/all');
    }

    updateCliente(cliente : Cliente){
      return this.httpClient.put(environment._API_SOLUTIONSCCC + '/cliente/' + cliente.idCliente, cliente);
    }

    deleteCliente(cliente : Cliente){
        return this.httpClient.delete(environment._API_SOLUTIONSCCC + '/cliente/' + cliente.idCliente);
    }

}