import { Component, OnInit } from '@angular/core';
import { LogginService } from 'src/app/services/loggin.service';
import { Routes, RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private logginService: LogginService, private router: Router) {
    this.logginService.activeSection(true);
   }

  ngOnInit(): void {
  }

  cerrarSection(){
   
    this.router.navigate(['']);
    
    
  }

}
