import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../components/header/header.component';
import { FooterComponent } from '../components/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { ClienteComponent } from './cliente/cliente.component';
import { NominaComponent } from './nomina/nomina.component';
import { PedidoComponent } from './pedido/pedido.component';
import { ProductoComponent } from './producto/producto.component';
import { ProductoPedidoComponent } from './producto-pedido/producto-pedido.component';
import { InventarioComponent } from './inventario/inventario.component';
import { ReporteComponent } from './reporte/reporte.component';
import { LandingPageComponent } from './landing-page/landing-page.component';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    EmpleadoComponent,
    ClienteComponent,
    NominaComponent,
    PedidoComponent,
    ProductoComponent,
    ProductoPedidoComponent,
    InventarioComponent,
    ReporteComponent,
    LandingPageComponent
  ],
  exports: [
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PagesModule { }
