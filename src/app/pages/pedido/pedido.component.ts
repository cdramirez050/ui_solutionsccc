import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { PedidoService } from 'src/app/services/pedido.service';
import { Cliente } from '../cliente/interface/cliente';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css']
})
export class PedidoComponent implements OnInit {

  public loaderSpinner: boolean = false;
  public loaderSpinnerList: boolean = false;
  public tableListPedidos: boolean = false;
  public editPedidoSelected: boolean = false;
  public campoUpdateDescripcion: boolean = false;
  public campoUpdateFechaPedido: boolean = false;
  public campoUpdateFechaEntrega: boolean = false;
  public campoUpdateValorTotal: boolean = false;
  public campoUpdateValorUnidad: boolean = false;
  public ocultarBuscarPorFechaPedido: boolean = false;
  public ocultarBuscarPorFechaEntrega: boolean = false;
  public ocultarBuscarPorDescripcion: boolean = false;
  public isBuscarPorFechaPedido: boolean = false;
  public isBuscarPorFechaEntrega: boolean = false;
  public isBuscarPorDescripcion: boolean = false;
  public checkTitle: boolean = false;
  public addPedido: boolean = true;
  public tableListPedido: boolean = false;
  public botonRegistro: boolean = false;
  public tableDeleteProduct: boolean = true;
  public selected: any;
  public fechaPedido: any;
  public fechaEntrega: any;
  public descripcionPedido: any;
  public catidad: any;
  public valorUnitario: any;
  public capturaValortotal: any;
  public vUnidad: any;
  public valorTotal: any;
  public listaCliente: any;
  public idCliente: any;
  public pedidoUpdate: any;
  public pedido: any;
  public optionMenu: any = [];
  public listAllPedidos: any = [];
  public pedidoEdit: any = [];
  public listForEliminar: any = [];
  public resultPedidoBuscado: any = [];

  constructor(
      private clienteService: ClienteService,
      private pedidoService: PedidoService
    ) 
    {
      this.optionMenu = this.menuPedido();
   }

  ngOnInit(): void {
  }

  menuPedido(){
    let menu;
    menu = [
      { name: 'Registro pedido'},
      { name: 'Lista pedido'},
      { name: 'Buscar pedido'},
    ]
    return menu
  }

  selectionSection(index: number){
    this.editPedidoSelected = false;
    this.tableListPedidos = false;
    this.botonRegistro = false;
    this.loaderSpinner = true;
    this.tableDeleteProduct = false;
    this.ocultarBuscarPorFechaPedido = false;
    this.ocultarBuscarPorFechaEntrega = false;
    this.ocultarBuscarPorDescripcion = false;
    this.selected = null;
    setTimeout(()=>{                           
      this.loaderSpinner = false;
      this.selected = index;
      this.checkTitle = false;
    }, 2000);
  }

  capturaFechaPedido(event: any){
    this.fechaPedido = event.target.value;
  }

  capturaFechaEntrega(event: any){
    this.fechaEntrega = event.target.value;
  }

  capturaDescripcionPedido(event: any){
    this.descripcionPedido = event.target.value;
  }
  
  capturaCantidad(event: any){
    this.catidad = event.target.value;
  }
  
  capturaValorPorUnidad(event: any){
    this.valorUnitario = event.target.value;
    let multiplicacion = parseInt(this.catidad) * parseInt(this.valorUnitario);
    this.valorTotal = multiplicacion.toString();
  }

  capturaValorTotal(event: any){
    this.capturaValortotal = event.target.value;
  }

  capturaVVUnidad(event: any){
    this.vUnidad =  event.target.value;
  }

  agregateCliente(){
    this.tableListPedido = true;
    this.clienteService.getAllClientes().subscribe((data)=>{
      console.log(data);
      this.listaCliente = data;
    })
  }

  addCliente(cliente: Cliente){
    this.botonRegistro = true;
    this.idCliente = cliente.idCliente;
    this.addPedido = false;
    this.tableListPedido = false;
  }

  savePedido(){
    this.pedido = {
      "fechaPedido": this.fechaPedido,
      "fechaEntrega": this.fechaEntrega,
      "descripcion": this.descripcionPedido,
      "cantidad": this.catidad,
      "valorUnidad": this.valorUnitario,
      "valorTotal": this.valorTotal,
      "isEstado": false,
      "cliente":[
        {
          "idCliente": this.idCliente
        }
      ]  
    }
    this.pedidoService.savePedido(this.pedido).subscribe((data)=>{
      console.log(data);
    },
    registro =>{
      this.botonRegistro = false;
      this.addPedido = true;
      (<HTMLFormElement>document.getElementById("registrar")).reset();
      alert("El pedido se guardo correctamente!!!!!");
    });
  }

  listaPedidos(){
    this.tableListPedidos = false;
    this.loaderSpinnerList = true;
    this.editPedidoSelected = false;
    this.pedidoService.getAllPedido().subscribe((data)=>{
      this.listAllPedidos = data;
    })
    setTimeout(()=>{                           
      this.loaderSpinnerList = false;
      this.tableListPedidos = true;
    }, 2000);
  }
  
  editPedido(pedido: any){
    this.campoUpdateDescripcion = false;
    this.campoUpdateFechaPedido = false;
    this.campoUpdateFechaEntrega = false;
    this.campoUpdateValorTotal = false;
    this.campoUpdateValorUnidad = false;
    this.pedidoService.getByIdPedido(pedido).subscribe((data)=>{
      this.pedidoEdit = [data];
    })
    this.loaderSpinnerList = true;
    this.tableListPedidos = false;
    setTimeout(()=>{                           
      this.loaderSpinnerList = false;
      this.editPedidoSelected = true;
    }, 2000);
  }

  activarUpdateDescripcion(){
    this.campoUpdateDescripcion = true;
    this.campoUpdateFechaPedido = false;
    this.campoUpdateFechaEntrega = false;
    this.campoUpdateValorTotal = false;
    this.campoUpdateValorUnidad = false;
  }

  activarUpdateFechaPedido(){
    this.campoUpdateFechaPedido = true;
    this.campoUpdateDescripcion = false;
    this.campoUpdateFechaEntrega = false;
    this.campoUpdateValorTotal = false;
    this.campoUpdateValorUnidad = false;
  }

  activarUpdateFechaEntrega(){
    this.campoUpdateFechaEntrega = true;
    this.campoUpdateFechaPedido = false;
    this.campoUpdateDescripcion = false;
    this.campoUpdateValorTotal = false;
    this.campoUpdateValorUnidad = false;
  }

  activarUpdateValorTotal(){
    this.campoUpdateValorTotal = true;
    this.campoUpdateFechaEntrega = false;
    this.campoUpdateFechaPedido = false;
    this.campoUpdateDescripcion = false;
    this.campoUpdateValorUnidad = false;
  }

  activarUpdateValorUnidad(){
    this.campoUpdateValorUnidad = true;
    this.campoUpdateValorTotal = false;
    this.campoUpdateFechaEntrega = false;
    this.campoUpdateFechaPedido = false;
    this.campoUpdateDescripcion = false;
  }

  guardarEditado(pedido: any){
    this.tableListPedidos = false;
    if (this.campoUpdateDescripcion){
      this.pedidoUpdate = {
        "idPedido": pedido.idPedido,
        "descripcion": this.descripcionPedido,
        "fechaPedido": pedido.fechaPedido,
        "fechaEntrega": pedido.fechaEntrega,
        "valorUnidad": pedido.valorUnidad,
        "valorTotal": pedido.valorTotal,
        "estado": pedido.estado
      }
    } else if (this.campoUpdateFechaEntrega) {
      this.pedidoUpdate = {
        "idPedido": pedido.idPedido,
        "descripcion": pedido.descripcion,
        "fechaPedido": pedido.fechaPedido,
        "fechaEntrega":  this.fechaEntrega,
        "valorUnidad": pedido.valorUnidad,
        "valorTotal": pedido.valorTotal,
        "estado": pedido.estado
      }
    } else if (this.campoUpdateFechaPedido) {
      this.pedidoUpdate = {
        "idPedido": pedido.idPedido,
        "descripcion": pedido.descripcion,
        "fechaPedido": this.fechaPedido,
        "fechaEntrega": pedido.fechaEntrega,
        "valorUnidad": pedido.valorUnidad,
        "valorTotal": pedido.valorTotal,
        "estado": pedido.estado
      }
    } else if (this.campoUpdateValorTotal){
      this.pedidoUpdate = {
        "idPedido": pedido.idPedido,
        "descripcion": pedido.descripcion,
        "fechaPedido": pedido.fechaPedido,
        "fechaEntrega": pedido.fechaEntrega,
        "valorUnidad": pedido.valorUnidad,
        "valorTotal": this.capturaValortotal,
        "estado": pedido.estado
      }
    } else {
      this.pedidoUpdate = {
        "idPedido": pedido.idPedido,
        "descripcion": pedido.descripcion,
        "fechaPedido": pedido.fechaPedido,
        "fechaEntrega": pedido.fechaEntrega,
        "valorUnidad": this.vUnidad,
        "valorTotal": pedido.valorTotal,
        "estado": pedido.estado
      }
    }
    this.pedidoService.updatePedido(this.pedidoUpdate).subscribe((data)=>{
      (<HTMLFormElement>document.getElementById("editar")).reset();
      this.tableListPedidos = false;
    });
    this.loaderSpinnerList = true;
    this.editPedidoSelected = false;
    setTimeout(()=>{                           
      this.loaderSpinnerList = false;
    }, 2000);
    alert("El pedido se actualizo correctamente!!!!");
    (<HTMLFormElement>document.getElementById("editar")).reset();
  }

  activarBuscarPorFechaPedido(isBucarPorFechaPedido: boolean, 
    isBucarPorFechaEntrega: boolean, isBuscarPorDescripcion: boolean){
      this.isBuscarPorFechaPedido = isBucarPorFechaPedido;
      this.isBuscarPorFechaEntrega = isBucarPorFechaEntrega;
      this.isBuscarPorDescripcion = isBuscarPorDescripcion;
      this.ocultarBuscarPorFechaPedido = true;
      this.ocultarBuscarPorFechaEntrega = false;
      this.ocultarBuscarPorDescripcion = false;
      this.tableDeleteProduct = false;
  }

  activarBuscarPorFechaEntrega(isBucarPorFechaPedido: boolean, 
    isBucarPorFechaEntrega: boolean, isBuscarPorDescripcion: boolean){
      this.isBuscarPorFechaPedido = isBucarPorFechaPedido;
      this.isBuscarPorFechaEntrega = isBucarPorFechaEntrega;
      this.isBuscarPorDescripcion = isBuscarPorDescripcion;
      this.ocultarBuscarPorFechaPedido = false;
      this.ocultarBuscarPorFechaEntrega = true;
      this.ocultarBuscarPorDescripcion = false;
      this.tableDeleteProduct = false;
  }

  activarBuscarPorDescripcion(isBucarPorFechaPedido: boolean, 
    isBucarPorFechaEntrega: boolean, isBuscarPorDescripcion: boolean){
      this.isBuscarPorFechaPedido = isBucarPorFechaPedido;
      this.isBuscarPorFechaEntrega = isBucarPorFechaEntrega;
      this.isBuscarPorDescripcion = isBuscarPorDescripcion;
      this.ocultarBuscarPorFechaPedido = false;
      this.ocultarBuscarPorFechaEntrega = false;
      this.ocultarBuscarPorDescripcion = true;
      this.tableDeleteProduct = false;
  }

  buscadorPedido(){
    this.resultPedidoBuscado = []; 
    this.pedidoService.getAllPedido().subscribe((data)=>{
      this.listForEliminar = data;
      if(this.listForEliminar.length < 1 ){
        alert("No hay registro en base de datos");
      } else {
        if(this.isBuscarPorFechaPedido){
          this.listForEliminar.forEach((element: any) => {
            if(element.fechaPedido == this.fechaPedido){
              this.resultPedidoBuscado.push(element);
              this.tableDeleteProduct = true;
              (<HTMLFormElement>document.getElementById("buscar")).reset();
            } 
          });
        } else if (this.isBuscarPorFechaEntrega){
          this.listForEliminar.forEach((element: any) => {
            if(element.fechaEntrega == this.fechaEntrega){
              this.resultPedidoBuscado.push(element);
              this.tableDeleteProduct = true;
              (<HTMLFormElement>document.getElementById("buscar")).reset();
            }
          });
        } else if (this.isBuscarPorDescripcion){
          this.listForEliminar.forEach((element: any) => {
            if(element.descripcion == this.descripcionPedido){
              this.resultPedidoBuscado.push(element);
              this.tableDeleteProduct = true;
              (<HTMLFormElement>document.getElementById("buscar")).reset();
            } 
          });
        }
      }
    });
  }

}
