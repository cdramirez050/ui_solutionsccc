import { Cliente } from "../../cliente/interface/cliente";

export class Pedido {
    "idPedido": number;
    "fechaPedido": string;
    "fechaEntrega": string;
    "descripcion": string;
    "cantidad": string;
    "valorUnidad": number;
    "valorTotal": number;
    "isEstado": boolean;
    "cliente": Cliente[];  
}