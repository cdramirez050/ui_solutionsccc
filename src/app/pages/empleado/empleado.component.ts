import { Component, OnInit } from '@angular/core';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { NominaService } from 'src/app/services/nomina.service';
import { Nomina } from '../nomina/interface/nomina';
import { Empleado } from './interface/empleado';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  public tableListNomina: boolean = false;
  public botonRegistro: boolean = false;
  public addBotonNomina: boolean = true;
  public vistaFormRegistroEmp: boolean;
  public campoUpdateNombre: boolean = false;
  public campoUpdateApellidos: boolean = false;
  public campoUpdateTelefono: boolean = false;
  public campoUpdateCorreo: boolean = false;
  public campoUpdatefechaContrato: boolean = false;
  public campoUpdatefechaNacimiento: boolean = false;
  public campoUpdateRh: boolean = false;
  public campoUpdateCargo: boolean = false;
  public campoUpdateEstado: boolean = false;
  public campoUpdateNomina: boolean = false;
  public ocultarBuscarPorNombre: boolean = false;
  public ocultarBuscarPorDocumento: boolean = false;
  public tableDeleteEmpleado: boolean = false;
  public empleadoResult: boolean = false;
  public empleadoResultByNombre: boolean = false;
  public loaderSpinner: boolean;
  public loaderSpinnerList: boolean;
  public checkTitle: boolean;
  public estatic: boolean;
  public empleado: any;
  public editempleado: any;
  public listaNomina: any;
  public IdNomina: any;
  public numeroDocumentoEmpleado: any;
  public nombreEmpleado: any;
  public tipoDocumentoEmp: any;
  public apellidosEmpleado: any;
  public numeroTelefonoEmp: any;
  public cargoEmpleado: any;
  public correoEmp: any;
  public fechaContrato: any;
  public fechaNacimiento: any;
  public rhEmp: any;
  public elemntBorrar: any;
  public listaEmpleadoNomina: any;
  public selected: any;
  public tableListEmp: any;
  public editEmpSelected: any;
  public empleadoUpdate: any;
  public isBuscarPorDocumento: any;
  public isBuscarPorNombre: any;
  public empleadoEncontrado: any;
  public estado: any;
  public optionMenu: any = [];

  constructor(
      private empleadoservice: EmpleadoService,
      private nominaService: NominaService
    ) 
    { 
      this.optionMenu = this.menuEmpleados();
      this.estatic = false;
      this.loaderSpinner = false;
      this.loaderSpinnerList = false;
      this.checkTitle = true;
      this.tableListEmp = false;
      this.editEmpSelected = false;
      this.vistaFormRegistroEmp = false;
  }

  ngOnInit(): void {
  }

  selectionSection(index: number){
    this.ocultarBuscarPorDocumento = false;
    this.ocultarBuscarPorNombre = false;
    this.tableListNomina = false;
    this.editEmpSelected = false;
    this.addBotonNomina = true;
    this.botonRegistro = false;
    this.loaderSpinner = true;
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
    this.campoUpdatefechaContrato = false;
    this.campoUpdatefechaNacimiento = false;
    this.campoUpdateRh = false;
    this.campoUpdateCargo = false;
    this.campoUpdateEstado = false;
    this.tableListNomina = false;
    this.selected = null;
    setTimeout(()=>{                           
      this.loaderSpinner = false;
      this.selected = index;
      this.checkTitle = false;
    }, 2000);
  }

  menuEmpleados(){
    let menu;
    menu = [
      { name: 'Registro empleados'},
      { name: 'Lista empleados'},
      { name: 'Buscar empleados'},
    ]
    return menu
  }

  capturaNombreEmpleado(event: any){
    this.nombreEmpleado = event.target.value;
  }

  capturaApellidoEmpleado(event: any){
    this.apellidosEmpleado = event.target.value;
  }

  capturaTipoDocumento(event: any){
    this.tipoDocumentoEmp = event.target.value;
  }
  
  capturaDocumentoEmpleado(event: any){
    this.numeroDocumentoEmpleado = event.target.value;
  }
  
  capturaCargoEmpleado(event: any){
    this.cargoEmpleado = event.target.value;
  }

  capturaCorreoEmpleado(event: any){
    this.correoEmp = event.target.value;
  }

  capturaFechaContrato(event: any){
    this.fechaContrato = event.target.value;
  }

  capturaFechaNacimiento(event: any){
    this.fechaNacimiento = event.target.value;
  }

  capturaTelefonoEmpleado(event: any){
    this.numeroTelefonoEmp = event.target.value;
  }

  capturarhEmp(event: any){
    this.rhEmp = event.target.value;
  }

  capturaNumeroDoc(event: any){
    this.numeroDocumentoEmpleado = event.target.value;
  }

  capturaTipoEstado(event: any){
    if(event.target.value == "Activo"){
      this.estado = true;
    } else {
      this.estado = false;
    }
  }

  agregateNomina(){
    this.nominaService.getAllNomina().subscribe((data) => {
      console.log(data);
      this.listaNomina = data;
      this.tableListNomina = true;
      this.botonRegistro = true;
    })
  }

  addNomina(nomina: Nomina){
    this.IdNomina = nomina.idNomina;
    this.tableListNomina = false;
    this.addBotonNomina = false;
  }

  saveEmpleado() {
    this.empleado = {
      "apellidos": this.apellidosEmpleado,
      "cargo": this.cargoEmpleado,
      "email": this.correoEmp,
      "fechaContrato": this.fechaContrato,
      "fechaNacimineto": this.fechaNacimiento,
      "nombres": this.nombreEmpleado,
      "numeroDocumento": this.numeroDocumentoEmpleado,
      "numeroTelefono": this.numeroTelefonoEmp,
      "rh": this.rhEmp,
      "tipoDocumento": this.tipoDocumentoEmp,
      "estado": this.estado,
      "nomina":[
        {
          "idNomina": this.IdNomina
        }
      ]
    }
    this.empleadoservice.saveEmpleado(this.empleado).subscribe(
      (data) =>{
        console.log(data);
      },
      registro => {
        (<HTMLFormElement>document.getElementById("registrar")).reset();
        this.loaderSpinner = false;
        this.selected = 0;
        alert("Cliente se guardo correctamente!!!!!");
      }
    )
  }

  listaEmpleado(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
    this.campoUpdatefechaContrato = false;
    this.campoUpdatefechaNacimiento = false;
    this.campoUpdateRh = false;
    this.campoUpdateCargo = false;
    this.campoUpdateEstado = false;
    this.tableListNomina = false;
    this.empleadoservice.getAllEmpleado().subscribe
      (data =>{
      console.log(data)
      this.listaEmpleadoNomina = data;
    });
    this.loaderSpinnerList = true;
    this.editEmpSelected = false;
    setTimeout(()=>{
      this.loaderSpinnerList = false;
      this.tableListNomina = true;
    }, 2000);
  }

  updateEmpleado(numeroDocumento: string, empleado: Empleado){
    this.empleadoservice.getByNumeroDocumentoEmpleado(numeroDocumento).subscribe
    (data => {
      console.log("Empleado a Actualizar", [data]);
      this.editempleado = [data];
    });
    this.tableListNomina = false;
    this.loaderSpinnerList = true;
    this.tableListEmp = false;
    setTimeout(()=>{
      this.loaderSpinnerList = false;
      this.editEmpSelected = true;
    }, 2000);
  }

  guardarEditado(empleado : Empleado){
    this.tableListNomina = false;
    if(this.campoUpdateNombre){
      this.empleadoUpdate = {
        "idEmpleado": empleado.idEmpleado,
        "nombres": this.nombreEmpleado,
        "apellidos": empleado.apellidos,
        "fechaContrato": empleado.fechaContrato,
        "fechaNacimineto": empleado.fechaNacimineto,
        "numeroTelefono": empleado.numeroTelefono,
        "rh": empleado.rh,
        "cargo": empleado.cargo,
        "email": empleado.email,
        "estado": empleado.estado
        }
      } else if (this.campoUpdateApellidos){
        this.empleadoUpdate = {
          "idEmpleado": empleado.idEmpleado,
          "nombres": empleado.nombres,
          "apellidos": this.apellidosEmpleado,
          "fechaContrato": empleado.fechaContrato,
          "fechaNacimineto": empleado.fechaNacimineto,
          "numeroTelefono": empleado.numeroTelefono,
          "rh": empleado.rh,
          "cargo": empleado.cargo,
          "email": empleado.email,
          "estado": empleado.estado
        }
      } else if (this.campoUpdateTelefono){
        this.empleadoUpdate = {
          "idEmpleado": empleado.idEmpleado,
          "nombres": empleado.nombres,
          "apellidos": empleado.apellidos,
          "fechaContrato": empleado.fechaContrato,
          "fechaNacimineto": empleado.fechaNacimineto,
          "numeroTelefono": this.numeroTelefonoEmp,
          "rh": empleado.rh,
          "cargo": empleado.cargo,
          "email": empleado.email,
          "estado": empleado.estado
        }
      } else if (this.campoUpdatefechaContrato){
        this.empleadoUpdate = {
          "idEmpleado": empleado.idEmpleado,
          "nombres": empleado.nombres,
          "apellidos": empleado.apellidos,
          "fechaContrato": this.fechaContrato,
          "fechaNacimineto": empleado.fechaNacimineto,
          "numeroTelefono": empleado.numeroTelefono,
          "rh": empleado.rh,
          "cargo": empleado.cargo,
          "email": empleado.email,
          "estado": empleado.estado
        }
      } else if (this.campoUpdatefechaNacimiento){
        this.empleadoUpdate = {
          "idEmpleado": empleado.idEmpleado,
          "nombres": empleado.nombres,
          "apellidos": empleado.apellidos,
          "fechaContrato": empleado.fechaContrato,
          "fechaNacimineto": this.fechaNacimiento,
          "numeroTelefono": empleado.numeroTelefono,
          "rh": empleado.rh,
          "cargo": empleado.cargo,
          "email": empleado.email,
          "estado": empleado.estado
        }
      } else if (this.campoUpdateCorreo){
        this.empleadoUpdate = {
          "idEmpleado": empleado.idEmpleado,
          "nombres": empleado.nombres,
          "apellidos": empleado.apellidos,
          "fechaContrato": empleado.fechaContrato,
          "fechaNacimineto": empleado.fechaNacimineto,
          "numeroTelefono": empleado.numeroTelefono,
          "rh": empleado.rh,
          "cargo": empleado.cargo,
          "email": this.correoEmp,
          "estado": empleado.estado
        }
      } else if (this.campoUpdateCargo){
        this.empleadoUpdate = {
          "idEmpleado": empleado.idEmpleado,
          "nombres": empleado.nombres,
          "apellidos": empleado.apellidos,
          "fechaContrato": empleado.fechaContrato,
          "fechaNacimineto": empleado.fechaNacimineto,
          "numeroTelefono": empleado.numeroTelefono,
          "rh": empleado.rh,
          "cargo": this.cargoEmpleado,
          "email": empleado.email,
          "estado": empleado.estado
        }
      } else if (this.campoUpdateEstado){
          this.empleadoUpdate = {
            "idEmpleado": empleado.idEmpleado,
            "nombres": empleado.nombres,
            "apellidos": empleado.apellidos,
            "fechaContrato": empleado.fechaContrato,
            "fechaNacimineto": empleado.fechaNacimineto,
            "numeroTelefono": empleado.numeroTelefono,
            "rh": empleado.rh,
            "cargo": empleado.cargo,
            "email": empleado.email,
            "estado": this.estado
        }
       } else {
        this.empleadoUpdate = {
          "idEmpleado": empleado.idEmpleado,
          "nombres": empleado.nombres,
          "apellidos": empleado.apellidos,
          "fechaContrato": empleado.fechaContrato,
          "fechaNacimineto": empleado.fechaNacimineto,
          "numeroTelefono": empleado.numeroTelefono,
          "rh": this.rhEmp,
          "cargo": empleado.cargo,
          "email": empleado.email,
          "estado": empleado.estado,
          "nomina": empleado.nomina
        }
      }
      this.empleadoservice.updateEmpleado(this.empleadoUpdate).subscribe
      (data =>{
        console.log("Empleado Actualizado", [data]);
      (<HTMLFormElement>document.getElementById("editar")).reset();
      this.tableListNomina = false;
      });
      this.loaderSpinnerList = true;
      this.editEmpSelected = false;
      setTimeout(()=>{                           
       this.loaderSpinnerList = false;
     }, 2000);
     alert("El Empleado se actualizo correctamente!!!!");
     (<HTMLFormElement>document.getElementById("editar")).reset();
  } 
  
  activarUpdateNombre(){
    this.campoUpdateNombre = true;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
    this.campoUpdatefechaContrato = false;
    this.campoUpdatefechaNacimiento = false;
    this.campoUpdateRh = false;
    this.campoUpdateCargo = false;
    this.campoUpdateEstado = false;
    this.tableListNomina = false;
  }
 
  activarUpdateApellido(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = true;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
    this.campoUpdatefechaContrato = false;
    this.campoUpdatefechaNacimiento = false;
    this.campoUpdateRh = false;
    this.campoUpdateCargo = false;
    this.campoUpdateEstado = false;
    this.tableListNomina = false;
  }

  activarUpdateTelefono(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = true;
    this.campoUpdateCorreo = false;
    this.campoUpdatefechaContrato = false;
    this.campoUpdatefechaNacimiento = false;
    this.campoUpdateRh = false;
    this.campoUpdateCargo = false;
    this.campoUpdateEstado = false;
    this.tableListNomina = false;
  }

  activarUpdateCorreo(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = true;
    this.campoUpdatefechaContrato = false;
    this.campoUpdatefechaNacimiento = false;
    this.campoUpdateRh = false;
    this.campoUpdateCargo = false;
    this.campoUpdateEstado = false;
    this.tableListNomina = false;
  }

  activarUpdateRh(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
    this.campoUpdatefechaContrato = false;
    this.campoUpdatefechaNacimiento = false;
    this.campoUpdateRh = true;
    this.campoUpdateCargo = false;
    this.campoUpdateEstado = false;
    this.tableListNomina = false;
  }

  activarUpdateFechaContrato(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
    this.campoUpdatefechaContrato = true;
    this.campoUpdatefechaNacimiento = false;
    this.campoUpdateRh = false;
    this.campoUpdateCargo = false;
    this.campoUpdateEstado = false;
    this.tableListNomina = false;
  }

  activarUpdateFechaNacimiento(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
    this.campoUpdatefechaContrato = false;
    this.campoUpdatefechaNacimiento = true;
    this.campoUpdateRh = false;
    this.campoUpdateCargo = false;
    this.campoUpdateEstado = false;
    this.tableListNomina = false;
  }

  activarUpdateCargo(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
    this.campoUpdatefechaContrato = false;
    this.campoUpdatefechaNacimiento = false;
    this.campoUpdateRh = false;
    this.campoUpdateCargo = true;
    this.campoUpdateEstado = false;
    this.tableListNomina = false;
  }

  activarUpdateEstado(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
    this.campoUpdatefechaContrato = false;
    this.campoUpdatefechaNacimiento = false;
    this.campoUpdateRh = false;
    this.campoUpdateCargo = false;
    this.campoUpdateEstado = true;
    this.tableListNomina = false;
  }

  activarBuscarPordocumento(isBuscarDocumento: boolean, isBuscarPorNombre: boolean){
    this.isBuscarPorDocumento = isBuscarDocumento;
    this.isBuscarPorNombre = isBuscarPorNombre;
    this.ocultarBuscarPorDocumento = true;
    this.ocultarBuscarPorNombre = false;
    this.tableDeleteEmpleado = false;
  }

  activarBuscarPorNombre(isBuscarDocumento: boolean, isBuscarPorNombre: boolean){
    this.isBuscarPorDocumento = isBuscarDocumento;
    this.isBuscarPorNombre = isBuscarPorNombre;
    this.ocultarBuscarPorNombre = true;
    this.ocultarBuscarPorDocumento = false;
    this.tableDeleteEmpleado = false;
  }

  buscadorEmpleado(){
    if(this.isBuscarPorDocumento){
      this.empleadoservice.getAllEmpleado().subscribe
        (data=>{
          this.listaEmpleadoNomina = data;
          if(this.listaEmpleadoNomina.length < 1){
            alert("No hay registro en base de datos");
            this.tableDeleteEmpleado = false;
          } else {
            this.listaEmpleadoNomina.forEach((element: any) => { 
              if(element.numeroDocumento == this.numeroDocumentoEmpleado){
                this.empleadoResult = true;
              }
            });
            if(this.empleadoResult){
              this.empleadoservice.getByNumeroDocumentoEmpleado(this.numeroDocumentoEmpleado).subscribe
              (data=>{
                console.log("Cliente Consultado por documento", [data]);
                (<HTMLFormElement>document.getElementById("buscar")).reset();
                this.tableDeleteEmpleado = true;
                this.ocultarBuscarPorDocumento = false;
                this.empleadoEncontrado = [data];
                this.empleadoResult = false;
              });
            } else {
              this.tableDeleteEmpleado = false;
              alert("Numero de documento no encontrado");
              this.ocultarBuscarPorDocumento = false;
            }   
          }
      });
    } else {
      this.empleadoservice.getAllEmpleado().subscribe
        (data=>{
          this.listaEmpleadoNomina = data;
          if(this.listaEmpleadoNomina.length < 1){
            alert("No hay registro en base de datos");
            this.tableDeleteEmpleado = false;
          } else {
            this.listaEmpleadoNomina.forEach((ele: any) => {
              if(ele.nombres == this.nombreEmpleado){
                this.empleadoResultByNombre = true;
              }
            });
            if(this.empleadoResultByNombre){
              this.empleadoservice.getByNombreEmpleado(this.nombreEmpleado).subscribe
              (data=>{
                console.log("Cliente Consultado por Nombre", [data]);
                (<HTMLFormElement>document.getElementById("buscar")).reset();
                this.tableDeleteEmpleado = true;
                this.ocultarBuscarPorNombre = false;
                this.empleadoEncontrado = [data];
                this.empleadoResultByNombre = false;
              });
            } else {
              this.tableDeleteEmpleado = false;
              alert("Nombre no encontrado");
              this.ocultarBuscarPorNombre = false;
            }
          }
      });
    }
  }

  eliminarEmpleado(empleado: Empleado){
    alert("Seguro desea eliminar el empleado");
    this.empleadoservice.deleteEmpleado(empleado).subscribe(
    (data) =>{
      console.log(data);
    },
    eliminar => {
      alert("Empleado se elimino correctamente!!!!!");
      this.tableDeleteEmpleado = false;
      this.ocultarBuscarPorDocumento = false;
      this.ocultarBuscarPorNombre = false;
    })
  }

}
