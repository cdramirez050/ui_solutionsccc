import { Nomina } from "../../nomina/interface/nomina";

export class Empleado {
    "idEmpleado": number;
    "apellidos": string;
    "cargo": string;
    "email": string;
    "fechaContrato": string;
    "fechaNacimineto": string;
    "nombres": string;
    "numeroDocumento": string;
    "numeroTelefono": string;
    "rh": string;
    "tipoDocumento": string;
    "estado": boolean;
    "nomina": Nomina[];  
}