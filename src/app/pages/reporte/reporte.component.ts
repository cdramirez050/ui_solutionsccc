import { Component, OnInit } from '@angular/core';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { PedidoService } from 'src/app/services/pedido.service';
import { PedidoProductoService } from 'src/app/services/pedidoProducto.service';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.css']
})
export class ReporteComponent implements OnInit {

  public loaderSpinner: boolean = false;
  public bannerProducto: boolean = false;
  public verDetalleEmpelado: boolean = false;
  public loaderSpinnerDos: boolean = false;
  public encabezadoReport: boolean = false;
  public butonVerDetalle: boolean = false;
  public activeSumaEngreso: boolean = false;
  public activeSumaIngreso: boolean = false;
  public filterPedidoProductos: any = [];
  public filterEmpleadosActivos: any = [];
  public filterPedidos: any = [];
  public listaUno: any = [];
  public listProductoPrint: any = [];
  public productos: any = [];
  public totalEngresos: any = [];
  public totalEgresoProducto: number = 0;
  public sumaEngresos: number = 0;
  public sumaIngresos: number = 0
  public sumaNomina: number = 0;
  public PorcentajeEgreso: number = 0;
  public PorcentajeIngreso: number = 0;
  public fechaReporte: any;
  public idPedidoFilter: any;
  public printTotalEngreso: any;
  public fechaReporteInit: any;
  public totalIngreso: any;
  
  constructor(
    private pedidoProcuctoService: PedidoProductoService,
    private empleadoService: EmpleadoService,
    private pedidoService: PedidoService
  ) { }

  ngOnInit(): void {
  }
  
  capturaFechaReporteInicio(event: any){
    this.fechaReporteInit = event.target.value;
  }
  
  capturaFechaReporte(event: any){
    this.filterPedidos = [];
    this.filterPedidoProductos = [];
    this.filterEmpleadosActivos = [];
    this.listaUno = [];
    this.fechaReporte = event.target.value;
    this.getPedido();
    this.getEmpleadoNomina();
    this.encabezadoReport = true;
    this.butonVerDetalle = true;
    alert("Generar el reporte");
    this.loaderSpinner = true;
    this.bannerProducto = false;
    setTimeout(()=>{                           
      this.loaderSpinner = false;
      this.bannerProducto = true;
    }, 2000);
  }

  getPedidoProducto(){
    this.pedidoProcuctoService.getAllCantidadProducto().subscribe((data)=>{
      let response: any;
      response = data;
      this.filterPedidos.forEach((element: any) => {
        response.forEach((res:any) => {
          if(element.idPedido.toString() == res.pedidoProductoss){
            this.filterPedidoProductos.push(res)
          }
        });
      });
      this.verDetalle();
      console.log("Consulta Pedido producto", this.filterPedidoProductos);
    })
  }

  getEmpleadoNomina(){
    this.empleadoService.getAllEmpleado().subscribe((data)=>{
      let response: any;
      response = data;
      response.forEach((element: any) => {
        if(element.estado){
          this.filterEmpleadosActivos.push(element)
        }
      });
      console.log("Consulta Empelado con nomina", this.filterEmpleadosActivos);
    })
  }

  getPedido(){
    this.pedidoService.getAllPedido().subscribe((data)=>{
      let response: any;
      response = data;
      response.forEach((element: any) => {
        if(element.fechaPedido <= this.fechaReporte && element.fechaPedido >= this.fechaReporteInit){
          this.filterPedidos.push(element);
        }
      });
      console.log("Consulta todos los pedidos", this.filterPedidos);
      this.getPedidoProducto();
    })
    this.listaUno.push(
      this.filterPedidos,
    )
  }

  selectEmpleado(){
    this.loaderSpinnerDos = true;
    setTimeout(()=>{                           
      this.loaderSpinnerDos = false;
      this.verDetalleEmpelado = true;
    }, 2000);
  }

  verDetalle(){
    this.filterPedidos.forEach((element: any) => {
        this.filterPedidoProductos.forEach((result: any) => { 
          if(element.idPedido.toString() == result.pedidoProductoss){
            this.productos.push(result)
          }
        });
        element.productos = this.productos
        this.calculateTotalEngreso(element);
        this.productos = [];
    });
  }

  calculateTotalEngreso(pedido: any){
    this.totalEgresoProducto = 0;
    let calculo: number = 0;
    if(pedido.estado == true){
      pedido.productos.forEach((el: any) => {
        let cantidadVenta = el.cantProducto;
        let valorConstoProducto = el.producto[0].vUnidadCosto;
        calculo = parseInt(cantidadVenta) * parseInt(valorConstoProducto);
        this.totalEgresoProducto += calculo
      });
      this.printTotalEngreso = this.totalEgresoProducto.toString();
      pedido.sumaEgresos = this.totalEgresoProducto.toString();
      console.log("Calculo", this.printTotalEngreso);
    } else {
      pedido.sumaEgresos = "0"
    }
  }

  sumaEngreso(){
    this.activeSumaEngreso = true;
    this.activeSumaIngreso = false;
    this.calculatePorcentaje();
  }

  sumaIngreso(){
    this.activeSumaIngreso = true;
    this.activeSumaEngreso = false;
    this.calculatePorcentaje();
  }

  calculatePorcentaje(){
    this.totalEngresos = [];
    this.sumaEngresos = 0;
    this.sumaIngresos = 0;
    this.PorcentajeEgreso = 0;
    this.PorcentajeIngreso = 0;
    this.filterPedidos.forEach((element: any) => {
      let egreso = element.sumaEgresos;
      let egresoNumber = parseInt(egreso);
      let ingresoVlorTotal = element.valorTotal;
      let ingresoNumberVtoal = parseInt(ingresoVlorTotal)
      this.sumaEngresos += egresoNumber;
      this.sumaIngresos += ingresoNumberVtoal;
    });
    this.filterEmpleadosActivos.forEach((empl: any) => {
      let sNomina = empl.nomina[0].totalPagar;
      let calculoSnomina = parseInt(sNomina)
      this.sumaEngresos += calculoSnomina
    });
    this.totalIngreso = this.sumaIngresos - this.sumaEngresos;
    let aux = (this.sumaEngresos * 100);
    let resultadoEgreso = aux / this.sumaIngresos;
    this.PorcentajeEgreso = Math.trunc(resultadoEgreso);
    let resultadoIngreso =  100 - this.PorcentajeEgreso;
    this.PorcentajeIngreso = Math.trunc(resultadoIngreso);
    
    this.totalEngresos.push({
      totalEgreso: this.sumaEngresos.toString(),
      totalIngreso: this.totalIngreso.toString(),
      porcentajeEgreso:  this.PorcentajeEgreso.toString(),
      porcentajeIngreso: this.PorcentajeIngreso.toString()
    });
  }
}
