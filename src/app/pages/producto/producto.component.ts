import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/services/producto.service';
import { Producto } from './interface/producto';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  public loaderSpinner: boolean = false;
  public campoUpdateNombre: boolean = false;
  public campoUpdateDescripcion: boolean = false;
  public campoUpdateCantidad: boolean = false;
  public campoUpdateFechaCompra: boolean = false;
  public campoUpdateVTotalProducto: boolean = false;
  public campoUpdatevUnidadCosto: boolean = false;
  public campoUpdatevUnidadVenta: boolean = false;
  public ocultarBuscarPorNombre: boolean = false;
  public ocultarBuscarPorFechaCompra: boolean = false;
  public tableDeleteProducto: boolean = false;
  public productoResult: boolean = false;
  public productoResultByFechaCompra: boolean = false;
  public checkTitle: boolean = false;
  public tableListProducto: boolean = false;
  public loaderSpinnerList: boolean;
  public valorVentaUnidad: any = 0;
  public valorCompraUnidad: any = 0;
  public selected: any;
  public nombreProducto: any;
  public descripcion: any;
  public fechaCompra: any;
  public cantidad: any;
  public valorTotalPedido: any;
  public producto: any;
  public valorVenta: any;
  public listaProducto: any;
  public editProducto: any;
  public productoUpdate: any;
  public isBuscarNombre: any;
  public isBuscarFechaCompra: any;
  public productoEncontrado: any;
  public editProSelected: any;
  public optionMenu: any = [];

  constructor(private productoService: ProductoService) {
    this.optionMenu = this.menuProducto();
    this.loaderSpinner = false;
    this.loaderSpinnerList = false;
    this.editProSelected = false;
  }

  ngOnInit(): void {
  }

  menuProducto(){
    let menu;
    menu = [
      { name: 'Registro producto'},
      { name: 'Lista producto'},
      { name: 'Buscar producto'},
    ]
    return menu
  }

  selectionSection(index: number){
    this.loaderSpinner = true;
    this.tableListProducto = false;
    this.tableListProducto = false;
    this.editProSelected = false;
    this.campoUpdateNombre = false;
    this.campoUpdateDescripcion = false;
    this.campoUpdateCantidad = false;
    this.campoUpdateFechaCompra = false;
    this.campoUpdateVTotalProducto = false;
    this.campoUpdatevUnidadCosto = false;
    this.campoUpdatevUnidadVenta = false;
    this.ocultarBuscarPorNombre = false;
    this.ocultarBuscarPorFechaCompra = false;
    this.tableDeleteProducto = false;
    this.selected = null;
    setTimeout(()=>{                           
      this.loaderSpinner = false;
      this.selected = index;
      this.checkTitle = false;
    }, 2000);
  }

  capturaNombreProducto(event: any){
    this.nombreProducto = event.target.value;
  }

  capturaDescripcion(event: any){
    this.descripcion = event.target.value;
  }

  capturaFechacompra(event: any){
    this.fechaCompra = event.target.value;
  }
  
  capturaCantidad(event: any){
    this.cantidad = event.target.value;
  }

  capturaValorVentaUnidad(event: any){
    this.valorVenta = event.target.value;
  }
  
  capturaValorTotalPedido(event: any){
    this.valorTotalPedido = event.target.value;
  }

  capturaValorPorUnidad(event: any){
    this.valorCompraUnidad = event.target.value;
  }

  saveProducto(){
    this.producto = {
      "nombre": this.nombreProducto,
      "descripcion": this.descripcion,
      "cantidad": this.cantidad,
      "fechaCompra": this.fechaCompra,
      "vTotalProducto": this.valorTotalPedido,
      "vUnidadCosto": this.valorCompraUnidad,
      "vUnidadVenta": this.valorVenta
    }
    this.productoService.saveProducto(this.producto).subscribe((data)=>{
      console.log(data);
    },
    registro =>{
      (<HTMLFormElement>document.getElementById("registrar")).reset();
      alert("El producto se guardo correctamente!!!!!");
    });
  }

  listaProductoM(){
    this.loaderSpinnerList = true;
    this.tableListProducto = false;
    this.editProSelected = false;
    this.campoUpdateNombre = false;
    this.campoUpdateDescripcion = false;
    this.campoUpdateCantidad = false;
    this.campoUpdateFechaCompra = false;
    this.campoUpdateVTotalProducto = false;
    this.campoUpdatevUnidadCosto = false;
    this.campoUpdatevUnidadVenta = false;
    this.productoService.getAllProducto().subscribe(data =>{
      console.log(data)
      this.listaProducto = data;
    });
    setTimeout(()=>{                           
      this.loaderSpinnerList = false;
      this.tableListProducto = true;
    }, 2000);
  }

  updateProducto(idProducto: Producto, producto: Producto){
    this.productoService.getById(idProducto).subscribe
    (data => {
      console.log("Empleado a Actualizar", [data]);
      this.editProducto = [data];
    });
    this.loaderSpinnerList = true;
    this.tableListProducto = false;
    setTimeout(()=>{
      this.loaderSpinnerList = false;
      this.editProSelected = true;
    }, 2000);
  }

  guardarEditado(producto : Producto){
    this.tableListProducto = false;
    if(this.campoUpdateNombre){
      this.productoUpdate = {
        "idProducto": producto.idProducto,
        "nombre": this.nombreProducto,
        "descripcion": producto.descripcion,
        "cantidad": producto.cantidad,
        "fechaCompra": producto.fechaCompra,
        "vTotalProducto": producto.vTotalProducto,
        "vUnidadCosto": producto.vUnidadCosto,
        "vUnidadVenta": producto.vUnidadVenta
        }
      } else if (this.campoUpdateDescripcion){
        this.productoUpdate = {
          "idProducto": producto.idProducto,
          "nombre": producto.nombre,
          "descripcion": this.descripcion,
          "cantidad": producto.cantidad,
          "fechaCompra": producto.fechaCompra,
          "vTotalProducto": producto.vTotalProducto,
          "vUnidadCosto": producto.vUnidadCosto,
          "vUnidadVenta": producto.vUnidadVenta
        }
      } else if (this.campoUpdateCantidad){
        this.productoUpdate = {
          "idProducto": producto.idProducto,
          "nombre": producto.nombre,
          "descripcion": producto.descripcion,
          "cantidad": this.cantidad,
          "fechaCompra": producto.fechaCompra,
          "vTotalProducto": producto.vTotalProducto,
          "vUnidadCosto": producto.vUnidadCosto,
          "vUnidadVenta": producto.vUnidadVenta
        }
      } else if (this.campoUpdateFechaCompra){
        this.productoUpdate = {
          "idProducto": producto.idProducto,
          "nombre": producto.nombre,
          "descripcion": producto.descripcion,
          "cantidad": producto.cantidad,
          "fechaCompra": this.fechaCompra,
          "vTotalProducto": producto.vTotalProducto,
          "vUnidadCosto": producto.vUnidadCosto,
          "vUnidadVenta": producto.vUnidadVenta
        }
      } else if (this.campoUpdateVTotalProducto){
        this.productoUpdate = {
          "idProducto": producto.idProducto,
          "nombre": producto.nombre,
          "descripcion": producto.descripcion,
          "cantidad": producto.cantidad,
          "fechaCompra": producto.fechaCompra,
          "vTotalProducto": this.valorTotalPedido,
          "vUnidadCosto": producto.vUnidadCosto,
          "vUnidadVenta": producto.vUnidadVenta
        }
      } else if (this.campoUpdatevUnidadCosto){
        this.productoUpdate = {
          "idProducto": producto.idProducto,
          "nombre": producto.nombre,
          "descripcion": producto.descripcion,
          "cantidad": producto.cantidad,
          "fechaCompra": producto.fechaCompra,
          "vTotalProducto": producto.vTotalProducto,
          "vUnidadCosto": this.valorCompraUnidad,
          "vUnidadVenta": producto.vUnidadVenta
        }
      } else {
        this.productoUpdate = {
          "idProducto": producto.idProducto,
          "nombre": producto.nombre,
          "descripcion": producto.descripcion,
          "cantidad": producto.cantidad,
          "fechaCompra": producto.fechaCompra,
          "vTotalProducto": producto.vTotalProducto,
          "vUnidadCosto": producto.vUnidadCosto,
          "vUnidadVenta": this.valorVentaUnidad
        }     
      }
      this.productoService.updateProducto(this.productoUpdate).subscribe
      (data =>{
        console.log("Producto Actualizado", [data]);
      (<HTMLFormElement>document.getElementById("editar")).reset();
      this.tableListProducto = false;
      });
      this.loaderSpinnerList = true;
      this.editProSelected = false;
      setTimeout(()=>{                           
       this.loaderSpinnerList = false;
     }, 2000);
     alert("El Producto se actualizo correctamente!!!!");
     (<HTMLFormElement>document.getElementById("editar")).reset();
  }

  activarUpdateNombre(){
    this.campoUpdateNombre = true;
    this.campoUpdateDescripcion = false;
    this.campoUpdateCantidad = false;
    this.campoUpdateFechaCompra = false;
    this.campoUpdateVTotalProducto = false;
    this.campoUpdatevUnidadCosto = false;
    this.campoUpdatevUnidadVenta = false;
  }

  activarUpdateDescripcion(){
    this.campoUpdateNombre = false;
    this.campoUpdateDescripcion = true;
    this.campoUpdateCantidad = false;
    this.campoUpdateFechaCompra = false;
    this.campoUpdateVTotalProducto = false;
    this.campoUpdatevUnidadCosto = false;
    this.campoUpdatevUnidadVenta = false;
  }

  activarUpdateCantidad(){
    this.campoUpdateNombre = false;
    this.campoUpdateDescripcion = false;
    this.campoUpdateCantidad = true;
    this.campoUpdateFechaCompra = false;
    this.campoUpdateVTotalProducto = false;
    this.campoUpdatevUnidadCosto = false;
    this.campoUpdatevUnidadVenta = false;
  }

  activarUpdateFechaCompra(){
    this.campoUpdateNombre = false;
    this.campoUpdateDescripcion = false;
    this.campoUpdateCantidad = false;
    this.campoUpdateFechaCompra = true;
    this.campoUpdateVTotalProducto = false;
    this.campoUpdatevUnidadCosto = false;
    this.campoUpdatevUnidadVenta = false;
  }

  activarUpdateVTotalProducto(){
    this.campoUpdateNombre = false;
    this.campoUpdateDescripcion = false;
    this.campoUpdateCantidad = false;
    this.campoUpdateFechaCompra = false;
    this.campoUpdateVTotalProducto = true;
    this.campoUpdatevUnidadCosto = false;
    this.campoUpdatevUnidadVenta = false;
  }

  activarUpdatevUnidadCosto(){
    this.campoUpdateNombre = false;
    this.campoUpdateDescripcion = false;
    this.campoUpdateCantidad = false;
    this.campoUpdateFechaCompra = false;
    this.campoUpdateVTotalProducto = false;
    this.campoUpdatevUnidadCosto = true;
    this.campoUpdatevUnidadVenta = false;
  }

  activarUpdatevUnidadVenta(){
    this.campoUpdateNombre = false;
    this.campoUpdateDescripcion = false;
    this.campoUpdateCantidad = false;
    this.campoUpdateFechaCompra = false;
    this.campoUpdateVTotalProducto = false;
    this.campoUpdatevUnidadCosto = false;
    this.campoUpdatevUnidadVenta = true;
  }

  activarBuscarPorNombre(isBuscarNombre: boolean, isBuscarFechaCompra: boolean){
    this.isBuscarNombre = isBuscarNombre;
    this.isBuscarFechaCompra = isBuscarFechaCompra;
    this.ocultarBuscarPorNombre = true;
    this.ocultarBuscarPorFechaCompra = false;
    this.tableDeleteProducto = false;
  }

  activarBuscarPorFechaCompra(isBuscarNombre: boolean, isBuscarFechaCompra: boolean){
    this.isBuscarNombre = isBuscarNombre;
    this.isBuscarFechaCompra = isBuscarFechaCompra;
    this.ocultarBuscarPorNombre = false;
    this.ocultarBuscarPorFechaCompra = true;
    this.tableDeleteProducto = false;
  }

  buscadorProducto(){
    if(this.isBuscarNombre){
      this.productoService.getAllProducto().subscribe
        (data=>{
          this.listaProducto = data;
          if(this.listaProducto.length < 1){
            alert("No hay registro en base de datos");
            this.tableDeleteProducto = false;
          } else {
            this.listaProducto.forEach((element: any) => { 
              if(element.nombre == this.nombreProducto){
                this.productoResult = true;
              }
            });
            if(this.productoResult){
              this.productoService.getByNombre(this.nombreProducto).subscribe
              (data=>{
                console.log("Producto Consultado por Nombre", [data]);
                (<HTMLFormElement>document.getElementById("buscar")).reset();
                this.tableDeleteProducto = true;
                this.ocultarBuscarPorNombre = false;
                this.productoEncontrado = [data];
                this.productoResult = false;
              });
            } else {
              this.tableDeleteProducto = false;
              alert("Producto no encontrado");
              this.ocultarBuscarPorNombre = false;
            }   
          }
      });
    } else {
      this.productoService.getAllProducto().subscribe
        (data=>{
          this.listaProducto = data;
          if(this.listaProducto.length < 1){
            alert("No hay registro en base de datos");
            this.tableDeleteProducto = false;
          } else {
            this.listaProducto.forEach((ele: any) => {
              if(ele.fechaCompra == this.fechaCompra){
                this.productoResultByFechaCompra = true;
              }
            });
            if(this.productoResultByFechaCompra){
              this.productoService.getByFechaCompra(this.fechaCompra).subscribe
              (data=>{
                console.log("Producto Consultado por Fecha Compra", [data]);
                (<HTMLFormElement>document.getElementById("buscar")).reset();
                this.tableDeleteProducto = true;
                this.ocultarBuscarPorNombre = false;
                this.productoEncontrado = [data];
                this.productoResultByFechaCompra = false;
              });
            } else {
              this.tableDeleteProducto = false;
              alert("Fecha no encontrada");
              this.ocultarBuscarPorNombre = false;
            }
          }
      });
    }
  }
  
}
