export class Producto {
    "idProducto": number;
    "nombre": string;
    "descripcion": string;
    "cantidad": string;
    "fechaCompra": string;
    "vTotalProducto": number;
    "vUnidadCosto": number;
    "vUnidadVenta": number;
}