import { Component, OnInit } from '@angular/core';
import { LogginService } from 'src/app/services/loggin.service';
import { Routes, RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-loggin',
  templateUrl: './loggin.component.html',
  styleUrls: ['./loggin.component.css']
})
export class LogginComponent implements OnInit {

  public passWord: any = [];
  public usuario: any = [];
  public cambioPassword: boolean = false;
  public formLoggin: boolean = true;
  public captPassword: any;
  public isEstado: any;
  public captNewPassword: any;
  public updatePassword: any;
  public idloggin: any;
  public newPassword: any;
  public NewCompareTo: any;
  public captUsuario: any;

  constructor(private logginService: LogginService, private router: Router ) { }

  ngOnInit(): void {
  }

  capturaUsuario(event: any){
    this.captUsuario = event.target.value;
  }

  capturaPassword(event: any){
    this.captPassword = event.target.value;
  }

  capturaNewPassword(event: any){
    this.captNewPassword = event.target.value;
  }

  validatePassword(){
    this.logginService.getByPassword(1).subscribe((data)=>{
      let response: any;
      response = data;
      if(response.estado){
        this.newPassword = response.password;
        this.usuario = response.usuario;
        if( this.captPassword == this.newPassword && this.captUsuario == this.usuario){
          this.router.navigate(['/home']);
        } else {
          (<HTMLFormElement>document.getElementById("loggin")).reset();
          alert("Contraseña usuario incorrecto");
        }
      } else {
        this.passWord = response.password;
        this.isEstado = response.estado;
        this.idloggin = response.idLoggin;
        this.usuario = response.usuario;
        debugger
        if(this.captPassword == this.passWord && !this.isEstado && this.captUsuario == this.usuario){
          this.cambioPassword = true;
          alert("Por favor cambie la contraseña");
          this.formLoggin = false;
        } else {
          (<HTMLFormElement>document.getElementById("loggin")).reset();
          alert("Contraseña usuario incorrecto");
        } 
      } 
    })
  }

  changePassword(){
    this.updatePassword = {
      "idLoggin": this.idloggin,
      "password": this.captNewPassword,
      "estado": true
    }
    this.logginService.updatePassword(this.updatePassword).subscribe((data)=>{
    });
    this.cambioPassword = false;
    this.formLoggin = true;
  }

}
