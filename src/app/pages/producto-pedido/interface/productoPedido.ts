import { ventaProducto } from "src/app/services/cantidadProductoVenta.service";
import { Pedido } from "../../pedido/interface/pedido";

export class ProductoPedido {
    "idStock": number;
    "producto": ventaProducto[];
    "pedido": Pedido[];
}