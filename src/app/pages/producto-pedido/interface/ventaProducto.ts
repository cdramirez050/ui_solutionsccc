import { Producto } from "../../producto/interface/producto";

export class VentaProducto {
    "idVentaProducto": number;
    "pedidoProductoss": string;
    "cantProducto": string;
    "producto": Producto[];
}