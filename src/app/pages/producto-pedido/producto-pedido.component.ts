import { Component, OnInit } from '@angular/core';
import { ventaProducto } from 'src/app/services/cantidadProductoVenta.service';
import { PedidoService } from 'src/app/services/pedido.service';
import { PedidoProductoService } from 'src/app/services/pedidoProducto.service';
import { ProductoService } from 'src/app/services/producto.service';
import { Producto } from '../producto/interface/producto';

@Component({
  selector: 'app-producto-pedido',
  templateUrl: './producto-pedido.component.html',
  styleUrls: ['./producto-pedido.component.css']
})
export class ProductoPedidoComponent implements OnInit {

  public loaderSpinner: boolean = false;
  public tableListPedido: boolean = false;
  public loaderSpinnerList: boolean = false;
  public checkTitle: boolean = false;
  public tableCantidad: boolean = false;
  public tableListPedidoSelect: boolean = false;
  public productosAddList: any = [];
  public listaPedidoEnviar: any = [];
  public optionMenu: any = [];
  public listaPedido: any = [];
  public nameProductos: any = [];
  public pedidoSelect: any;
  public cantidadProducto: any;
  public cantidadVenProducto: any;
  public cantidadPedido: any;
  public selected: any;
  public updateEstadoPedido: any;
  public updateProducto: any;

  constructor(
    private pedidoservice: PedidoService,
    private productoService: ProductoService,
    private pedidoProductoService: PedidoProductoService,
    private ventaProductoService: ventaProducto,
    ) {
    this.optionMenu = this.menuProducto();
   }

  ngOnInit(): void {
  }

  menuProducto(){
    let menu;
    menu = [
      { name: 'Consultar pedidos pendintes'}
    ]
    return menu
  }

  selectionSection(index: number){
    this.loaderSpinner = true;
    this.selected = null;
    setTimeout(()=>{                           
      this.loaderSpinner = false;
      this.selected = index;
      this.checkTitle = false;
    }, 2000);
  }

  listaPedidos(){
    this.nameProductos = [];
    this.loaderSpinnerList = true; 
    this.tableListPedido = false;
    this.tableListPedidoSelect = false;
    this.tableCantidad = false;
    setTimeout(()=>{                           
      this.loaderSpinnerList = false;
    }, 2000);
    let pedidos: any = [];
    this.listaPedido = [];
    this.pedidoservice.getAllPedido().subscribe((data)=>{
      console.log("Pedidos", data);
      this.loaderSpinnerList = false;
      pedidos = data;
      pedidos.forEach((element: any)=> {
        if(!element.estado){
          this.listaPedido.push(element);
          console.log("Pedidos listados", this.listaPedido);
          this.tableListPedido = true;
        } 
      }); 
    })
  }

  addProductos(){
    this.tableCantidad = true;
    this.productoService.getAllProducto().subscribe((data)=>{
      console.log("Productos", data);
      let productos: any = [];
      productos = data;
      productos.forEach((element: any) => {
        this.nameProductos.push(element);
      });
    })
  }

  selectPedido(select: any){
    this.tableListPedido = false;
    this.tableListPedidoSelect = true;
    this.pedidoSelect = [select];
  }

  capturaCantidad(event: any){
    this.cantidadProducto = event.target.value;
  }

  saveCantidad(producto: Producto){
    let auxUno = parseInt(producto.cantidad);
    let auxDos = parseInt(this.cantidadProducto);
    let result = auxUno - auxDos;
    let updatecantidad = result.toString();
    this.updateProducto = {
      "idProducto": producto.idProducto,
      "cantidad": updatecantidad ,
      "fechaCompra": producto.fechaCompra,
      "vTotalProducto": producto.vTotalProducto,
      "vUnidadCosto": producto.vUnidadCosto,
      "vUnidadVenta": producto.vUnidadVenta
    }
    if(producto.cantidad == "0"){
      alert("La cantidad supera lo que existe");
    } else {
      (<HTMLFormElement>document.getElementById("registro")).reset();
      this.productoService.updateProducto(this.updateProducto).subscribe((data)=>{
        console.log("Producto Actualizado", [data]);
      })
      this.cantidadVenProducto = {
        "pedidoProductoss": this.pedidoSelect[0].idPedido.toString(),
        "cantProducto": this.cantidadProducto,
        "producto":[
          {
            "idProducto": producto.idProducto
          }
        ]
      }
      this.pedidoProductoService.saveCantidadProducto(this.cantidadVenProducto).subscribe(
        (data)=>{
          console.log(data);
        },
        registro =>{
          alert("Agregado!!!!!");
        }
      );
    }
  }
  
  saveCantidadPedido(){
    let allVentaProducto: any = [];
    this.listaPedidoEnviar = [];
    this.pedidoProductoService.getAllCantidadProducto().subscribe((data)=>{
      if (typeof data != 'undefined'){
        allVentaProducto = data;
        allVentaProducto.forEach((result: any) => {
          if(parseInt(result.pedidoProductoss) == this.pedidoSelect[0].idPedido){
            this.productosAddList.push(result);
          }
        });
        this.productosAddList.forEach((element: any) => {
          this.listaPedidoEnviar.push( {"idVentaProducto": element.idVentaProducto});
        });
        this.productosAddList = [];
        this.cantidadPedido = {
          "pedido":[{"idPedido": this.pedidoSelect[0].idPedido}],
          "producto": this.listaPedidoEnviar 
        }
        this.updateEstadoPedido = {
          "idPedido": this.pedidoSelect[0].idPedido,
          "fechaPedido": this.pedidoSelect[0].fechaPedido,
          "fechaEntrega": this.pedidoSelect[0].fechaEntrega,
          "descripcion": this.pedidoSelect[0].descripcion,
          "cantidad": this.pedidoSelect[0].cantidad,
          "valorUnidad": this.pedidoSelect[0].valorUnidad,
          "valorTotal": this.pedidoSelect[0].valorTotal,
          "estado": true
        }
        this.pedidoservice.updatePedido(this.updateEstadoPedido).subscribe((data)=>{
          console.log(data);
        });
        this.ventaProductoService.savePedidoProducto(this.cantidadPedido).subscribe((data)=>{
          console.log(data);
        },
        registro =>{
          (<HTMLFormElement>document.getElementById("registro")).reset();
          alert("Se agregaron productos al pedido correctamente!!!!!");
        });
        this.loaderSpinnerList = true;
        this.tableListPedido = false;
        this.tableListPedidoSelect = false;
        this.tableCantidad = false;
        setTimeout(()=>{                           
          this.loaderSpinnerList = false;
        }, 2000);
      }
    })
  }
}
