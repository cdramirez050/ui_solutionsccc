import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductoPedidoComponent } from './producto-pedido.component';

describe('ProductoPedidoComponent', () => {
  let component: ProductoPedidoComponent;
  let fixture: ComponentFixture<ProductoPedidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductoPedidoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductoPedidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
