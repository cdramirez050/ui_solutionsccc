import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { Cliente } from './interface/cliente';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})

export class ClienteComponent implements OnInit {

  public clienteResultByNombre: boolean = false;
  public campoUpdateNombre: boolean = false;
  public campoUpdateApellidos: boolean = false;
  public campoUpdateTelefono: boolean = false;
  public campoUpdateCorreo: boolean = false;
  public campoUpdateDireccion: boolean = false;
  public ocultarBuscarPorNombre: boolean = false;
  public ocultarBuscarPorDocumento: boolean = false;
  public tableDeleteClient: boolean = false;
  public clienteResult: boolean = false;
  public vistaFormRegistro: boolean;
  public loaderSpinner: boolean;
  public loaderSpinnerList: boolean;
  public checkTitle: boolean;
  public estatic: boolean
  public numeroDocumento: any;
  public nombreCliente: any;
  public tipoDocumento: any;
  public apellidoCliente: any;
  public numeroTelefono: any;
  public direccionCliente: any;
  public correoCliente: any;
  public elemntBorrar: any;
  public isBuscarPorDocumento: any;
  public isBuscarPorNombre: any;
  public clienteEdit: any;
  public clienteUpdate: any;
  public clienteEncontrado: any;
  public selected: any;
  public editClienteSelected: any; 
  public tableListClient: any;
  public clienteSelected: any = {};
  public optionMenu: any = [];
  public listaCliente: any = [];
  public cliente: any = {};

    
  constructor(private clienteService: ClienteService) {
    this.optionMenu = this.menuClientes();
    this.estatic = false;
    this.loaderSpinner = false;
    this.loaderSpinnerList = false;
    this.checkTitle = true;
    this.tableListClient = false;
    this.editClienteSelected = false;
    this.vistaFormRegistro = false;
   }

  ngOnInit(): void {
  }

  menuClientes(){
    let menu = [];
    menu = [
      { name: 'Registro cliente'},
      { name: 'Listar clientes'},
      { name: 'Buscar clientes'},
    ]
    return menu
  }

  capturaNombre(event: any){
    this.nombreCliente = event.target.value;
  }

  capturaApellido(event: any){
    this.apellidoCliente = event.target.value;
  }

  capturaTipoDoc(event: any){
    this.tipoDocumento =  event.target.value;
  }

  capturaNumeroDoc(event: any){
    this.numeroDocumento = event.target.value;
  }

  capturaTelefono(event: any){
    this.numeroTelefono = event.target.value;
  }

  capturaDireccion(event: any){
    this.direccionCliente = event.target.value;
  }

  capturaCorreo(event: any){
    this.correoCliente = event.target.value;
  }

  savePasajero() {  
    this.cliente = {
      "nombres": this.nombreCliente,
      "apellidos": this.apellidoCliente,
      "tipoDocumento": this.tipoDocumento,
      "numeroDocumento": this.numeroDocumento,
      "numeroTelefono": this.numeroTelefono,
      "direccion": this.direccionCliente,
      "correo": this.correoCliente,
    }
    this.clienteService.saveCliente(this.cliente).subscribe(
      (data) =>{
        console.log(data);
      },
      registro => {
        (<HTMLFormElement>document.getElementById("registro")).reset();
        alert("Cliente se guardo correctamente!!!!!");
      }
    )
  }

  listaClientes(){
    this.clienteService.getAllClientes().subscribe
    (data=>{
      console.log(data);
      this.listaCliente = data;
      if(this.listaCliente.length < 1){
        alert("No hay registro en base de datos");
        this.tableListClient = false;
        this.editClienteSelected = false;
      } else {
        this.listaCliente = data;
        this.tableListClient = true;
      }
    });
  }

  editarCliente(numeroDocumento : string, cliente : Cliente){
    this.clienteService.getByNumeroDocumento(numeroDocumento).subscribe
    (data=>{
      console.log("Cliente para Actualizar",[data]);
      this.clienteEdit = [data];
    });
    this.loaderSpinnerList = true;
    this.tableListClient = false;
    setTimeout(()=>{                           
      this.loaderSpinnerList = false;
      this.editClienteSelected = true;
    }, 2000);
  }

  guardarEditado(cliente : Cliente){
    this.tableListClient = false;
    if (this.campoUpdateNombre){
      this.clienteUpdate = {
        "idCliente": cliente.idCliente,
        "nombres": this.nombreCliente,
        "apellidos": cliente.apellidos,
        "numeroTelefono": cliente.numeroTelefono,
        "direccion": cliente.direccion,
        "correo": cliente.correo
      }
    } else if (this.campoUpdateApellidos) {
      this.clienteUpdate = {
        "idCliente": cliente.idCliente,
        "nombres": cliente.nombres,
        "apellidos": this.apellidoCliente,
        "numeroTelefono": cliente.numeroTelefono,
        "direccion": cliente.direccion,
        "correo": cliente.correo
      }
    } else if (this.campoUpdateTelefono) {
      this.clienteUpdate = {
        "idCliente": cliente.idCliente,
        "nombres": cliente.nombres,
        "apellidos": cliente.apellidos,
        "numeroTelefono": this.numeroTelefono,
        "direccion": cliente.direccion,
        "correo": cliente.correo
      }
    } else if (this.campoUpdateCorreo){
      this.clienteUpdate = {
        "idCliente": cliente.idCliente,
        "nombres": cliente.nombres,
        "apellidos": cliente.apellidos,
        "numeroTelefono": cliente.numeroTelefono,
        "direccion": cliente.direccion,
        "correo": this.correoCliente
      }
    } else {
      this.clienteUpdate = {
        "idCliente": cliente.idCliente,
        "nombres": cliente.nombres,
        "apellidos": cliente.apellidos,
        "numeroTelefono": cliente.numeroTelefono,
        "direccion": this.direccionCliente,
        "correo": cliente.correo
      }
    }
    this.clienteService.updateCliente(this.clienteUpdate).subscribe
    (data=>{
      console.log("Cliente Actualizado", [data]);
      (<HTMLFormElement>document.getElementById("registro")).reset();
      this.tableListClient = false;
    });
    this.loaderSpinnerList = true;
    this.editClienteSelected = false;
    setTimeout(()=>{                           
      this.loaderSpinnerList = false;
    }, 2000);
    alert("El Clinte se actualizo correctamente!!!!");
    (<HTMLFormElement>document.getElementById("editar")).reset();
    
    
  }

  buscadorCliente(){
    if(this.isBuscarPorDocumento){
      this.clienteService.getAllClientes().subscribe
        (data=>{
          this.listaCliente = data;
          if(this.listaCliente.length < 1){
            alert("No hay registro en base de datos");
            this.tableDeleteClient = false;
          } else {
            this.listaCliente.forEach((element: any) => { 
              if(element.numeroDocumento == this.numeroDocumento){
                this.clienteResult = true;
              }
            });
            if(this.clienteResult){
              this.clienteService.getByNumeroDocumento(this.numeroDocumento).subscribe
              (data=>{
                console.log("Cliente Consultado por documento", [data]);
                (<HTMLFormElement>document.getElementById("buscar")).reset();
                this.tableDeleteClient = true;
                this.ocultarBuscarPorDocumento = false;
                this.clienteEncontrado = [data];
                this.clienteResult = false;
              });
            } else {
              this.tableDeleteClient = false;
              alert("Numero de documento no encontrado");
              this.ocultarBuscarPorDocumento = false;
            }   
          }
      });
    } else {
      this.clienteService.getAllClientes().subscribe
        (data=>{
          this.listaCliente = data;
          if(this.listaCliente.length < 1){
            alert("No hay registro en base de datos");
            this.tableDeleteClient = false;
          } else {
            this.listaCliente.forEach((ele: any) => {
              if(ele.nombres == this.nombreCliente){
                this.clienteResultByNombre = true;
              }
            });
            if(this.clienteResultByNombre){
              this.clienteService.getByNombreCliente(this.nombreCliente).subscribe
              (data=>{
                console.log("Cliente Consultado por Nombre", [data]);
                (<HTMLFormElement>document.getElementById("buscar")).reset();
                this.tableDeleteClient = true;
                this.ocultarBuscarPorNombre = false;
                this.clienteEncontrado = [data];
                this.clienteResultByNombre = false;
              });
            } else {
              this.tableDeleteClient = false;
              alert("Nombre no encontrado");
              this.ocultarBuscarPorNombre = false;
            }
          }
      });
    }
  }

  eliminarCliente(cliente: Cliente){
    this.clienteService.deleteCliente(cliente).subscribe(
    (data) =>{
      console.log(data);
    },
    eliminar => {
      alert("Cliente se elimino correctamente!!!!!");
      this.tableDeleteClient = false;
      this.ocultarBuscarPorDocumento = false;
      this.ocultarBuscarPorNombre = false;
    })
  }

  selectionSection(index: number){
    this.loaderSpinner = true;
    this.selected = null;
    this.tableListClient = false;
    this.ocultarBuscarPorDocumento = false;
    this.ocultarBuscarPorNombre = false;
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
    setTimeout(()=>{                           
      this.loaderSpinner = false;
      this.selected = index;
      this.checkTitle = false;
    }, 2000);
  }

  activarBuscarPordocumento(isBuscarDocumento: boolean, isBuscarPorNombre: boolean){
    this.isBuscarPorDocumento = isBuscarDocumento;
    this.isBuscarPorNombre = isBuscarPorNombre;
    this.ocultarBuscarPorDocumento = true;
    this.ocultarBuscarPorNombre = false;
    this.tableDeleteClient = false;
  }

  activarBuscarPorNombre(isBuscarDocumento: boolean, isBuscarPorNombre: boolean){
    this.isBuscarPorDocumento = isBuscarDocumento;
    this.isBuscarPorNombre = isBuscarPorNombre;
    this.ocultarBuscarPorNombre = true;
    this.ocultarBuscarPorDocumento = false;
    this.tableDeleteClient = false;
  }

  activarUpdateNombre(){
    this.campoUpdateNombre = true;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
  }
 
  activarUpdateApellido(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = true;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
    this.campoUpdateDireccion = false;
  }

  activarUpdateTelefono(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = true;
    this.campoUpdateCorreo = false;
    this.campoUpdateDireccion = false;
  }

  activarUpdateCorreo(){
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = true;
    this.campoUpdateDireccion = false;
  }

  activarUpdateDireccion(){
    this.campoUpdateDireccion = true,
    this.campoUpdateNombre = false;
    this.campoUpdateApellidos = false;
    this.campoUpdateTelefono = false;
    this.campoUpdateCorreo = false;
  }
}

