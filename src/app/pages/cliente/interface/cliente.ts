export class Cliente {
    "idCliente": number;
    "nombres": string;
    "apellidos": string;
    "tipoDocumento": string;
    "numeroDocumento": string;
    "numeroTelefono": string;
    "direccion": string;
    "correo": string;
}