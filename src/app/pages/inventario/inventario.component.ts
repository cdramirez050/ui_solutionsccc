import { Component, OnInit } from '@angular/core';
import { InventarioService } from 'src/app/services/inventario.service';
import { ProductoService } from 'src/app/services/producto.service';
import { Producto } from '../producto/interface/producto';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.css']
})
export class InventarioComponent implements OnInit {

  public loaderSpinner: boolean = false;
  public tableListInventario: boolean = false;
  public loaderSpinnerList: boolean = false;
  public tableDetalleInventario: boolean = false;
  public tableBuscarInventario: boolean = false;
  public listadetalleBuscado: boolean = false;
  public tableListProductos: boolean = false;
  public checkTitle: boolean = false;
  public botonRegistro: boolean = false; 
  public optionMenu: any = [];
  public inventarioSelected: any = [];
  public elementEncontrado: any = [];
  public listAddProducto: any = [];
  public listaAllInventarios: any = [];
  public selected: any;
  public fechaInventario: any;
  public fechaToBuscar: any;
  public listaProducto: any;
  public saveInvent: any;

  constructor(
    private productoService: ProductoService, 
    private inventarioService: InventarioService
    ) {
    this.optionMenu = this.menuProducto();
   }

  ngOnInit(): void {
  }

  menuProducto(){
    let menu;
    menu = [
      { name: 'Registro inventario'},
      { name: 'Lista inventarios'},
      { name: 'Buscar inventario'},
    ]
    return menu
  }

  selectionSection(index: number){
    this.tableDetalleInventario = false;
    this.tableListProductos = false;
    this.tableListInventario = false;
    this.botonRegistro = false;
    this.loaderSpinner = true;
    this.selected = null;
    setTimeout(()=>{                           
      this.loaderSpinner = false;
      this.selected = index;
      this.checkTitle = false;
    }, 2000);
  }

  capturaFechaPedido(event: any){
    this.fechaInventario = event.target.value;
  }

  capturaFechaInicio(event: any){
    this.loaderSpinnerList = true;
    this.listadetalleBuscado = false;
    setTimeout(()=>{                           
      this.loaderSpinnerList = false;
      this.listadetalleBuscado = true;
    }, 2000);
    this.elementEncontrado = [];
    this.fechaToBuscar = event.target.value;
    this.inventarioService.getAllInventario().subscribe((data)=>{
      let result: any = [];
      result = data;
      result.forEach((element: any) => {
        if(element.fechaInventario == this.fechaToBuscar){
          this.elementEncontrado.push(element);
        } 
      });
      if(this.elementEncontrado.length == 0){
        alert("No se encontro resultado para la fecha seleccionada");
      }
    })
  }

  agregateProducto(){
    this.productoService.getAllProducto().subscribe((data)=>{
      this.listaProducto = data;
    })
    this.tableListProductos = true;
    this.botonRegistro = true;
  }

  addProducto(producto: Producto){
    this.listAddProducto.push({"idProducto": producto.idProducto});
    alert("Porducto añadido");
  }

  saveInventario(){
    this.loaderSpinnerList = true;
    if(this.listAddProducto.length == 0){
      alert("Por favor agrege porductos al inventario");
    } else {
      this.saveInvent = {
        "fechaInventario": this.fechaInventario,
        "producto": this.listAddProducto
      }
      this.inventarioService.saveInventario(this.saveInvent).subscribe((data)=>{
        console.log(data);
      },
      registro=>{
        this.listAddProducto = [];
        this.tableListProductos = false;
        this.botonRegistro = false;
        setTimeout(()=>{                           
          this.loaderSpinnerList = false;
        }, 2000);
        alert("Inventario se genero correctamente!!!!!");
      })
    }
  }

  listaInventario(){
    this.tableDetalleInventario = false;
    this.tableListProductos = false;
    this.tableListInventario = false;
    this.botonRegistro = false;
    this.loaderSpinnerList = true;
    this.inventarioService.getAllInventario().subscribe((data)=>{
      console.log(data);
      this.listaAllInventarios = data;
      if(this.listaAllInventarios.length < 1){
        alert("No hay registro en base de datos");
        this.tableListInventario = false;
        this.loaderSpinnerList = false;
      } else {
        setTimeout(()=>{                           
          this.loaderSpinnerList = false;
          this.tableListInventario = true;
        }, 2000);
      }
    })
  }

  verDetalle(fechaInventario: string){
    this.inventarioSelected = [];
    this.loaderSpinnerList = true;
    this.tableListInventario = false;
    this.listaAllInventarios.forEach((element: any) => {
      if(fechaInventario == element.fechaInventario){
        this.inventarioSelected.push(element);
      }
    });
    if( this.inventarioSelected.length == 0){
      alert("Este inventario no tiene productos registrados");
    }
    setTimeout(()=>{                           
      this.loaderSpinnerList = false;
      this.tableDetalleInventario = true;
    }, 2000);
  }

  eliminarInventario(inventario: any){
    alert("Seguro desea eliminar inventario");
    this.loaderSpinnerList = true;
    this.tableListInventario = false;
    this.inventarioService.deleteInventario(inventario).subscribe((data) =>{
      alert("inventario se elimino");
    })
    setTimeout(()=>{                           
      this.loaderSpinnerList = false;
    }, 2000);
  }

}
