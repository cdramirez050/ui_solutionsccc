import { Producto } from "../../producto/interface/producto";

export class Inventario {
    "idInventario": number;
    "fechaInventario": string;
    "producto": Producto[];
}