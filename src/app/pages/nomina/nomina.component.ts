import { Component, OnInit } from '@angular/core';
import { NominaService } from 'src/app/services/nomina.service';
import { Nomina } from './interface/nomina';

@Component({
  selector: 'app-nomina',
  templateUrl: './nomina.component.html',
  styleUrls: ['./nomina.component.css']
})
export class NominaComponent implements OnInit {

  public nominaResult: boolean = false;
  public nominaResultSalario: boolean = false;
  public campoUpdateNombre: boolean = false;
  public campoUpdateSalario: boolean = false;
  public tableListNomina: boolean = false;
  public tableDeleteNomina: boolean = false;
  public ocultarBuscarPorNombre: boolean = false;
  public ocultarBuscarPorSalario: boolean = false;
  public loaderSpinner: boolean = false;
  public checkTitle: boolean = false;
  public loaderSpinnerList: boolean;
  public selected: any;
  public nominaCapture: any;
  public salario: any;
  public nomina: any;
  public nominaUpdate: any;
  public listaNomina: any;
  public editNomina: any;
  public editNomSelected: any;
  public isBuscarNombre: any;
  public isBuscarSalario: any;
  public nominaEncontrada: any;
  public optionMenu: any = [];
 

  constructor(private nominaService : NominaService) { 
    this.optionMenu = this.menuNomina();
    this.loaderSpinner = false;
    this.loaderSpinnerList = false;
    this.editNomSelected = false;
  }

  ngOnInit(): void {
  }

  menuNomina(){
    let menu;
    menu = [
      { name: 'Registro Nomina'},
      { name: 'Lista Nomina'},
      { name: 'Buscar Nomina'},
    ]
    return menu
  }

  selectionSection(index: number){
    this.editNomSelected = false;
    this.tableListNomina = false;
    this.campoUpdateNombre = false;
    this.campoUpdateSalario = false;
    this.ocultarBuscarPorNombre = false;
    this.ocultarBuscarPorSalario = false;
    this.loaderSpinner = true;
    this.selected = null;
    setTimeout(()=>{                           
      this.loaderSpinner = false;
      this.selected = index;
      this.checkTitle = false;
    }, 2000);
  }

  capturaNombreNomina(event: any){
    this.nominaCapture = event.target.value;
  }

  capturaSalario(event: any){
    this.salario = event.target.value;
  }

  saveNomina(){
    this.nomina = {
      "nombre": this.nominaCapture,
      "totalPagar": this.salario
    }
    this.nominaService.saveNomina(this.nomina).subscribe((data) =>{
      console.log(data);
    },
    nomina => {
      (<HTMLFormElement>document.getElementById("registro")).reset();
      alert("La nomina se guardo correctamente!!!!!");
    });
  }

  listaNominaM(){
    this.editNomSelected = false;
    this.loaderSpinnerList = true;
    this.campoUpdateNombre = false;
    this.campoUpdateSalario = false;
    this.nominaService.getAllNomina().subscribe
      (data =>{
      console.log(data)
      this.listaNomina = data;
    });
    setTimeout(()=>{
      this.loaderSpinnerList = false;
      this.tableListNomina = true;
    }, 2000);
  }

  updateCliente(idNomina: Nomina, nomina: Nomina){
    this.nominaService.getById(idNomina).subscribe
    (data => {
      this.editNomina = [data];
    });
    this.loaderSpinnerList = true;
    this.tableListNomina = false;
    setTimeout(()=>{
      this.loaderSpinnerList = false;
      this.editNomSelected = true;
    }, 2000);
  }

  activarUpdateNombre(){
    this.campoUpdateNombre = true;
    this.campoUpdateSalario = false;
  }

  activarUpdateSalario(){
    this.campoUpdateNombre = false;
    this.campoUpdateSalario = true;
  }

  guardarEditado(nomina : Nomina){
    this.tableListNomina = false;
    if(this.campoUpdateNombre){
      this.nominaUpdate = {
        "idNomina": nomina.idNomina,
        "nombre": this.nominaCapture,
        "totalPagar": nomina.totalPagar
        }
      } else {
        this.nominaUpdate = {
          "idNomina": nomina.idNomina,
          "nombre": nomina.nombre,
          "totalPagar": this.salario
        }
      }
    this.nominaService.updateCliente(this.nominaUpdate).subscribe
    (data =>{
      console.log("Nomina Actualizada", [data]);
    (<HTMLFormElement>document.getElementById("editar")).reset();
    this.tableListNomina = false;
    });
    this.loaderSpinnerList = true;
    this.editNomSelected = false;
    setTimeout(()=>{                           
      this.loaderSpinnerList = false;
    }, 2000);
    alert("Nomina se actualizó correctamente!!!!");
    (<HTMLFormElement>document.getElementById("editar")).reset();
    }

    activarBuscarPorNombre(isBuscarNombre: boolean, isBuscarSalario: boolean){
    this.isBuscarNombre = isBuscarNombre;
    this.isBuscarSalario = isBuscarSalario;
    this.ocultarBuscarPorNombre = true;
    this.ocultarBuscarPorSalario = false;
    this.tableDeleteNomina = false;
    }

    activarBuscarPorSalario(isBuscarSalario: boolean, isBuscarNombre: boolean){
      this.isBuscarNombre = isBuscarNombre;
      this.isBuscarSalario = isBuscarSalario;
      this.ocultarBuscarPorNombre = false;
      this.ocultarBuscarPorSalario = true;
      this.tableDeleteNomina = false;
    }

    buscadorNomina(){
      if(this.isBuscarNombre){
        this.nominaService.getAllNomina().subscribe
          (data=>{
            this.listaNomina = data;
            if(this.listaNomina.length < 1){
              alert("No hay registro en base de datos");
              this.tableDeleteNomina = false;
            } else {
              this.listaNomina.forEach((element: any) => { 
                if(element.nombre == this.nominaCapture){
                  this.nominaResult = true;
                }
              });
              if(this.nominaResult){
                this.nominaService.getByNombre(this.nominaCapture).subscribe
                (data=>{
                  console.log("Nómina Consultado por Nombre", [data]);
                  (<HTMLFormElement>document.getElementById("buscar")).reset();
                  this.tableDeleteNomina = true;
                  this.ocultarBuscarPorNombre = false;
                  this.nominaEncontrada = [data];
                  this.nominaResult = false;
                });
              } else {
                this.tableDeleteNomina = false;
                alert("Nómina no encontrada");
                this.ocultarBuscarPorNombre = false;
              }   
            }
        });
      } else {
        this.nominaService.getAllNomina().subscribe
          (data=>{
            this.listaNomina = data;
            if(this.listaNomina.length < 1){
              alert("No hay registro en base de datos");
              this.tableDeleteNomina = false;
            } else {
              this.listaNomina.forEach((ele: any) => {
                if(ele.totalPagar == this.salario){
                  this.nominaResultSalario = true;
                }
              });
              if(this.nominaResultSalario){
                this.nominaService.getByNombre(this.nominaCapture).subscribe
                (data=>{
                  (<HTMLFormElement>document.getElementById("buscar")).reset();
                  this.tableDeleteNomina = true;
                  this.ocultarBuscarPorSalario = false;
                  this.nominaEncontrada = [data];
                  this.nominaResultSalario = false;
                });
              } else {
                this.tableDeleteNomina = false;
                alert("Fecha no encontrada");
                this.ocultarBuscarPorNombre = false;
              }
            }
        });
      }
    }

    eliminarNomina(nomina: Nomina){
      
      alert("Seguro desea eliminar la nomina");
      this.nominaService.deleteNomina(nomina).subscribe(
      (data) =>{
        console.log("Edu",data);
      },
      eliminar => {
        if(eliminar.error.statusCode == 500){
          alert("No se puede eliminar, nómina registrada a empleado")
          this.tableDeleteNomina = false;
          this.ocultarBuscarPorNombre = false;
          this.ocultarBuscarPorSalario = false;
        } else {
          alert("Nómina se elimino correctamente!!!!!");
          this.tableDeleteNomina = false;
          this.ocultarBuscarPorNombre = false;
          this.ocultarBuscarPorSalario = false;
        }
                
      })
    }

}
