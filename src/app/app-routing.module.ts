import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { EmpleadoComponent } from './pages/empleado/empleado.component';
import { HomeComponent } from './pages/home/home.component';
import { InventarioComponent } from './pages/inventario/inventario.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { LogginComponent } from './pages/loggin/loggin.component';
import { NominaComponent } from './pages/nomina/nomina.component';
import { PedidoComponent } from './pages/pedido/pedido.component';
import { ProductoPedidoComponent } from './pages/producto-pedido/producto-pedido.component';
import { ProductoComponent } from './pages/producto/producto.component';
import { ReporteComponent } from './pages/reporte/reporte.component';



const routes: Routes = [
  {
    path: '',
    component: LogginComponent
  },
  {
    path: 'quejas',
    component: LandingPageComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'empleados',
    component: EmpleadoComponent
  },
  {
    path: 'clientes',
    component: ClienteComponent
  },
  {
    path: 'nomina',
    component: NominaComponent
  },
  {
    path: 'pedido',
    component: PedidoComponent
  },
  {
    path: 'producto',
    component: ProductoComponent
  },
  {
    path: 'productopedido',
    component: ProductoPedidoComponent
  },
  {
    path: 'inventario',
    component: InventarioComponent
  },
  {
    path: 'reportes',
    component: ReporteComponent
  }
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
